# Evreux-discover

## Présentation

Bienvenue sur Evreux-discover ! Ce projet a été réalisé dans le cadre du cours de NSI (Numérique et Sciences Informatiques) en classe de première.

### Objectif

L'objectif de ce projet est de créer une carte interactive permettant de découvrir les lieux incontournables de la ville d'Evreux, en France. Cette carte offre des informations complémentaires et détaillées sur chaque lieu, ainsi que des recoins et endroits insoupçonnés à explorer. Elle met également en avant les toilettes publiques et les points d'eau gratuits.

## Utilisation

Pour utiliser Evreux-discover, il vous suffit d'ouvrir le fichier `index.html` dans votre navigateur. Vous pouvez alors explorer la carte interactive et découvrir les différents lieux d'Evreux.

## Licence

Ce projet est sous licence MIT. Veuillez consulter le fichier `LICENSE` pour plus d'informations.
